const each = (elements, call_back) => {
  if (!Array.isArray(elements)) {
    console.log("Input is not an array. Please give proper input");
    return;
  }

  for (let index = 0; index < elements.length; index++) {
    let ele = elements[index];
    call_back(ele, index);
  }
  return elements;
};

module.exports = each;
