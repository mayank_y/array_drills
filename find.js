function find(array, call_back) {
  for (let i = 0; i < array.length; i++) {
    if (call_back(array[i])) {
      return array[i];
    }
  }
}

module.exports = find;
