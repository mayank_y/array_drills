let filter = (array, call_back) => {
  let output = [];
  for (let index = 0; index < array.length; index++) {
    let bool = call_back(array[index]);
    if (bool) {
      output.push(array[index]);
    }
  }
  return output;
};

module.exports = filter;
