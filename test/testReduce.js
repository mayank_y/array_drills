const reduce = require("../reduce");
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

const result = reduce(
  items,
  (a, b) => {
    a.push(b * b);
    return a;
  },
  []
);
console.log(result);
