let filter = require("../filter");
const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

const result = filter(items, (a) => {
  return a > 4;
});

console.log(result);
