const each = require("../each");

const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

each(items, (a, b) => {
  console.log(a, b);
});
