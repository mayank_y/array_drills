const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.
const find = require("../find");

let result = find(items, (element) => {
  return element > 2;
});

console.log(result);
