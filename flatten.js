const flatten = (array) => {
  let output = [];
  for (let index = 0; index < array.length; index++) {
    if (Array.isArray(array[index])) {
      let loc_arr = flatten(array[index]);
      output.push(...loc_arr);
    } else {
      output.push(array[index]);
    }
  }
  return output;
};

module.exports = flatten;
