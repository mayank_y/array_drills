let map = (array, call_back) => {
  let output = [];
  for (let index = 0; index < array.length; index++) {
    let num = call_back(array[index]);
    output.push(num);
  }
  //console.log("returning", output);
  return output;
};

module.exports = map;
