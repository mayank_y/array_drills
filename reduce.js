function reduce(array, call_back, accumulator) {
  let first = accumulator,
    start = 0;
  if (
    typeof first == "NaN" ||
    typeof first == "undefined" ||
    typeof first == "null"
  ) {
    first = array[0];
    start = 1;
  }

  for (let i = start; i < array.length; i++) {
    second = array[i];
    first = call_back(first, second);
  }
  return first;
}

module.exports = reduce;
